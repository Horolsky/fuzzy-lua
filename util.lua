
local function deepcopy(t, seen)

    if type(t) ~= 'table' then return t end
    if seen and seen[t] then return seen[t] end

    local s = seen or {}
    local res = {}
    s[t] = res
    for k, v in next, t do res[deepcopy(k, s)] = deepcopy(v, s) end
    return setmetatable(res, getmetatable(t))
end

local function minpair(t)
    local keymin, valuemin = nil, nil

    for key, value in pairs(t) do
        keymin, valuemin = key, value
        break
    end

    for key, value in pairs(t) do
        pcall(function ()
            if value < valuemin then
                keymin, valuemin = key, value
            end
        end)
    end

    return keymin, valuemin
end

local function maxpair(t)
    local keymax, valuemax = nil, nil

    for key, value in pairs(t) do
        keymax, valuemax = key, value
        break
    end

    for key, value in pairs(t) do
        pcall(function ()
            if value > valuemax then
                keymax, valuemax = key, value
            end
        end)
    end

    return keymax, valuemax
end

local function range (...)
    local args = {...}
    local start, stop, step = ({
        [3] = function () return table.unpack(args) end,
        [2] = function () return args[1], args[2], 1 end,
        [1] = function () return       1, args[1], 1 end,
    })[#args]()

    return function(_, last)
        if last >= stop then
            return nil
        else
            return last + step
        end
    end, nil, start - step
end

local util = {
    deepcopy = deepcopy,
    minpair = minpair,
    maxpair = maxpair,
    range = range
}

return util
