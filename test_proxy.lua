#! /usr/bin/env lua

require('except')
require('unittesting')
local proxy = require('proxy')

local TS = TestSuite:new("Test proxy.lua")

TS:test("create proxytable",
    function ()
        local t = proxy.proxytable({1,2,3})
        assert(t[1] == 1)
        assert(t[2] == 2)
        assert(t[3] == 3)
    end
)

TS:test("check proxytable update when original changes",
    function ()
        local orig = {1,2,3}
        local t = proxy.proxytable(orig)

        orig[0] = 0
        assert(t[0] == 0, "proxy __newindex handling failed")

        orig[1] = 42
        assert(t[1] == 42, "proxy __index handling failed")
    end
)

TS:test("check proxy.countable for observing all keys",
    function ()
        local orig = {1,2,3}
        local t = proxy.countable(orig)

        assert(#orig == 3)
        assert(#t == 3)

        orig[0] = 42
        assert(#orig == 3)
        assert(#t == 4)
    end
)

TS:test("check proxy.immutable for throwing an error",
    function ()
        local t = proxy.immutable({1,2,3})

        local sts, res = pcall(function ()
            t[0] = 42
        end)
        assert(t[0] == nil, "immutability handler failed")
        assert(false == sts, "error wasn't thrown as expected")
        assert(getmetatable(res) == Exception, "expected Exception trown")
        assert((res or {}).kind == Exception.kind.Base, "expected Exception.kind.Base trown")
    end
)

TS:test("check proxy.compose with all policies",
    function ()

        local t = proxy.compose(proxy.immutable, proxy.countable, proxy.printable)({1,"test string",3})

        -- test printable
        assert(string.find(tostring(t),"test string"))

        -- test countable
        assert(#t == 3)

        -- test immutable
        local sts, res = pcall(function ()
            t[0] = 42
        end)
        assert(false == sts, "error wasn't thrown as expected")
    end
)


os.exit(TS:run())
