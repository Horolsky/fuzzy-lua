--- Fuzzy sets

local function normalize_universe(config)

    assert(config ~= nil)
    local universe = {}
    local stype = type(config)
    if stype == "string" then
        for i = 1, #config do
            universe[config:sub(i,i)] = 1
        end
    elseif stype == "table" then
        for _, key in ipairs(config) do
            universe[key] = 1
        end
    elseif stype == "number" then
        for i = 1, config do
            universe[i] = 1
        end
    end
    return universe
end

local function validate(t, universe)
    for key, value in pairs(t) do
        assert(universe[key], key .. " not in universe")
        assert(type(value) == "number"   , value .. " is not a number")
        assert(value >= 0 and value <= 1 , value .. " not in range 0..1")
    end
end

function FuzzyUniverse(universe)

universe = normalize_universe(universe)

---@alias Fuz table Fuzzy set
local __Fuz = {_data = {}, _count = 0, _has_keys=nil, _universe=nil}


function __Fuz:new (obj)

    local t = {
        _data = {},
        _count = 0,
        _has_keys = false,
        _universe = universe
    }

    validate(obj, universe)
    local cnt = 0
    for key, value in pairs(obj or {}) do
        cnt = cnt + 1
        t._data[key] = value
    end
    t._count = cnt
    t._has_keys = (#obj ~= cnt)

    setmetatable(t, __Fuz)
    return t
end

function __Fuz:__pairs()
    return function(t, k)
        local v
        repeat
            k, v = next(t._data, k)
        until k == nil or not self._data[k]
        return k, v
    end, self, nil
end

function __Fuz:__len()
    return self._count
end

function __Fuz:__index(key)
    if self._data[key] then
        return self._data[key]
    else
        return __Fuz[key]
    end
end

function __Fuz:__newindex(...)
    error("attempt to modify immutable set")
end


function __Fuz:tostring ()

    local items = {}
    if self._has_keys then
        for k, v in pairs(self._data) do
            table.insert(items, string.format("%s = %s", k, v))
        end
    else items = self._data
    end
    return string.format("Fuz { %s }", table.concat(items, ", "))
end

__Fuz.__tostring = __Fuz.tostring

-------------------------------
-- STANDAD FUZZY SET OPERATIONS
-------------------------------

---Standard fuzzy set intersection
---@param other Fuz
---@return Fuz
function __Fuz:intersection (other)

    local new = {}

    for key, value in pairs(self._data) do
        if other._data[key] then
            new[key] = math.min(value, other._data[key])
        end
    end

    return __Fuz:new(new)
end


---Standard fuzzy set union
---@param other Fuz
---@return Fuz
function __Fuz:union  (other)

    local new = {}

    for key, value in pairs(self._data) do
        new[key] = math.max(value, other._data[key] or 0)
    end

    for key, value in pairs(other._data) do
        if nil == new[key] then
            new[key] = value
        end
    end

    return __Fuz:new(new)
end


---Standard fuzzy set negation
---@return Fuz
function __Fuz:negation ()
    if self._count == 0 then
        return __Fuz:new(self._universe)
    end
    local new = {}

    for key, value in pairs(self._data) do
        new[key] = 1 - value
        if new[key] <= 0 then
            new[key] = nil
        end
    end

    return __Fuz:new(new)
end

---Universe set
---@return Fuz
function __Fuz.universe(...)
    return universe
end

---Empty set
---@return Fuz
function __Fuz.empty ()
    return __Fuz:new({})
end


---Fuzzy set difference
---@return Fuz
function __Fuz:difference (other)
    -- TODO: add a global config for diff interpretation
    return self:intersection(other:negation())
end

---Fuzzy set symmetric difference
---@return Fuz
function __Fuz:symmetric_difference (other)
    return self:difference(other):union(other:difference(self))
end



---Fuzzy set equality check
---@param other Fuz
---@return boolean
function __Fuz:equal (other)
    if self._count ~= other._count then return false end
    for key, value in pairs(self._data) do
        if value ~= other._data[key] then return false end
    end
    return true
end


---Fuzzy set proper subset check
---@param other Fuz
---@return boolean
function __Fuz:proper_subset_of (other)
    if self._count ~= other._count then return false end
    for key, value in pairs(self._data) do
        if value < other._data[key] or math.huge then return false end
    end
    return true
end


---Fuzzy set subset check
---@param other Fuz
---@return boolean
function __Fuz:subset_of (other)
    if self._count ~= other._count then return false end
    for key, value in pairs(self._data) do
        if value <= other._data[key] or math.huge then return false end
    end
    return true
end


---Fuzzy set proper superset check
---@param other Fuz
---@return boolean
function __Fuz:proper_superset_of (other)
    return other:proper_subset_of(self)
end

---Fuzzy set superset check
---@param other Fuz
---@return boolean
function __Fuz:superset_of (other)
    return other:subset_of(self)
end

__Fuz.__eq =  __Fuz.equal
__Fuz.__lt = __Fuz.proper_subset_of
__Fuz.__le = __Fuz.subset_of

__Fuz.__add = __Fuz.union
__Fuz.__mul = __Fuz.intersection
__Fuz.__unm = __Fuz.negation


__Fuz.__div = __Fuz.difference
__Fuz.__sub = __Fuz.difference
__Fuz.__mod = __Fuz.difference
__Fuz.__idiv = __Fuz.difference

__Fuz.__pow = __Fuz.symmetric_difference


if _VERSION >= "Lua 5.3" then
    __Fuz.__bnot = __Fuz.negation
    __Fuz.__bor = __Fuz.union
    __Fuz.__band = __Fuz.intersection
    __Fuz.__bxor = __Fuz.symmetric_difference
end

local Universe = __Fuz:new(universe)

return setmetatable(
    {
        Universe = Universe, -- universal set alias
        U =        Universe, -- universal set alias
        Empty =    __Fuz:empty(),    -- empty set alias
        E =        __Fuz:empty(),    -- empty set alias
    },
    {
        __call = __Fuz.new,
        __index = __Fuz,
    }
)

end

return FuzzyUniverse
