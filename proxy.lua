
---------------------------
-- proxy table maker module
---------------------------

require('except')
local util = require('util')

---
--- Create proxy table
---
--- A basic proxy table redirects member calls to the given original table t.
--- The proxy metatable is created as a deep copy from the original metatable.
---
---@param t any
---@return table proxy
---@nodiscard
local function proxytable(t)
    local mt = getmetatable(t) or {}
    mt = util.deepcopy(mt)
    mt.__index = t
    mt.__newindex = function(_, key, value)
        t[key] = value
    end
    mt.__len = function(_)
        return #t
    end
    mt.__pairs = function (_)
        return pairs(t)
    end
    return setmetatable({}, mt)
end

---
--- Create immutable proxy
---
---@param t any
---@return table proxy
local function immutable(t)
    local proxy = proxytable(t)
    local mt = getmetatable(proxy)
    mt.__newindex = function(_, key, value)
        throw("attempt to modify an immutable object")
    end
    return proxy
end

---Create countable proxy
---
---@param t any
---@return table proxy
local function countable(t)
    local proxy = proxytable(t)
    local mt = getmetatable(proxy)
    mt.__len = function(_)
        local count = 0
        for _ in pairs(t) do
            count = count + 1
        end
        return count
    end
    return proxy
end


---Create printable proxy
---@param t any
---@return table proxy
local function printable(t)
    local proxy = proxytable(t)
    local mt = getmetatable(proxy)
    mt.__tostring = function(_)
        local str = (t.__name or "table") .. " {"
        for k, v in pairs(t) do
            str = str .. "\n\t" .. k .. ":\t" .. tostring(v)
        end
        return str .. "\n}"
    end
    return proxy
end


---Compose proxy policies
---@param ... function
---@return function
local function compose(...)
    local policies = {...}
    return function (t)
        for _, policy in pairs(policies) do
            t = policy(t)
        end
        return t
    end
end

--#endregion PROXY

local proxy = {
    proxytable = proxytable,
    immutable = immutable,
    countable = countable,
    printable = printable,
    compose = compose
}

return proxy
