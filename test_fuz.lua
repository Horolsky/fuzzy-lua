#! /usr/bin/env lua

package.path = './?.lua;' .. package.path
require('except')
require('unittesting')

local FuzzyUniverse = require('fuz')

local TS = TestSuite:new("Test fuz.lua")

-- local Fuz = fuz.Fuz

TS:test("initialize with universe size",
    function ()
        local Fuz = FuzzyUniverse(3)
        assert(pcall(function () Fuz:new({.1,.2,.3})                end ))
        assert(pcall(function () Fuz {.1,.2,.3}                     end ))
    end
)

TS:test("initialize with universe string",
    function ()
        local Fuz = FuzzyUniverse('abc')
        assert(pcall(function () Fuz:new({a = .1, b = .2, c = .3})  end ))
        assert(pcall(function () Fuz {a = .1, b = .2, c = .3}       end ))
    end
)

TS:test("initialize with universe key list",
    function ()
        local Fuz = FuzzyUniverse({'a', 'b', 'c'})
        assert(pcall(function () Fuz:new({a = .1, b = .2, c = .3})  end ))
        assert(pcall(function () Fuz {a = .1, b = .2, c = .3}       end ))
    end
)

TS:test("invalid initialization",
    function ()
        local Fuz = FuzzyUniverse(3)
        assert(not pcall(function () Fuz:new({1, 2, 3}) end ))
        assert(not pcall(function () Fuz {a = .1, b = .2, c = .3} end ))

        Fuz = FuzzyUniverse('abc')
        assert(not pcall(function () Fuz:new({"a", "b"}) end ))
        assert(not pcall(function () Fuz {.1,.2,.3} end ))
    end
)

TS:test("operator overloads",
    function ()
        local Fuz = FuzzyUniverse(0)
        local A = Fuz {}

        assert(pcall(function () return A + A end ))
        assert(pcall(function () return A * A end ))
        assert(pcall(function () return -A end ))

        assert(pcall(function () return A / A end ))
        -- assert(pcall(function () return A // A end ))
        -- assert(pcall(function () return A % A end ))
        assert(pcall(function () return A - A end ))
        assert(pcall(function () return A ^ A end ))

        if _VERSION >= "Lua 5.3" then
            assert(pcall(function () return A and A end ))
            assert(pcall(function () return A & A end ))
            assert(pcall(function () return A or A end ))
            assert(pcall(function () return A | A end ))
        end
    end
)

TS:test("non-elimination intersection",
    function ()
        local Fuz = FuzzyUniverse(3)

        local A = Fuz:new({.1,.2,.3})
        local B = Fuz:new({.1,.5,.6})
        local C = A:intersection(B)

        assert(C[1] == math.min(A[1], B[1]))
        assert(C[2] == math.min(A[2], B[2]))
        assert(C[3] == math.min(A[3], B[3]))
    end
)

TS:test("elimination intersection",
    function ()
        local Fuz = FuzzyUniverse(3)

        local A = Fuz:new({.1,.2,.3})
        local B = Fuz:new({.5,.6})
        local C = A:intersection(B)

        assert(C[1] == math.min(A[1], B[1]))
        assert(C[2] == math.min(A[2], B[2]))
        assert(C[3] == nil)
        assert(#C == 2)
    end
)

TS:test("non-expansion union",
    function ()
        local Fuz = FuzzyUniverse(3)

        local A = Fuz:new({.1,.2,.3})
        local B = Fuz:new({.1,.5,.6})
        local C = A:union(B)

        assert(C[1] == math.max(A[1], B[1]))
        assert(C[2] == math.max(A[2], B[2]))
        assert(C[3] == math.max(A[3], B[3]))
    end
)

TS:test("expansion union",
    function ()
        local Fuz = FuzzyUniverse('abcd')

        local A = Fuz:new({a = .1, b = .2, c = .3})
        local B = Fuz:new({        b = .1, c = .2, d = .3})
        local C = A:union(B)

        assert(C.a == A.a)
        assert(C.d == B.d)
        assert(C.b == math.max(A.b, B.b))
        assert(C.c == math.max(A.c, B.c))
        assert(#C == 4)
    end
)

TS:test("non-eliminating negation",
    function ()
        local Fuz = FuzzyUniverse(3)

        local A = Fuz:new({.1, .2, .3})
        local B = A:negation()

        assert(B[1] == 1 - A[1])
        assert(B[2] == 1 - A[2])
        assert(B[3] == 1 - A[3])
    end
)

TS:test("eliminating negation",
    function ()
        local Fuz = FuzzyUniverse(4)

        local A = Fuz:new({.1, .2, .3, 1})
        local B = A:negation()

        assert(B[1] == 1 - A[1])
        assert(B[2] == 1 - A[2])
        assert(B[3] == 1 - A[3])
        assert(B[4] == nil)
        assert(#B == 3)
    end
)

TS:test("universe and empty set constants",
    function ()
        local Fuz = FuzzyUniverse(4)

        -- assert(Fuz.U:negation() == Fuz.E)
        -- assert(Fuz.E:negation() == Fuz.U)

        local A = Fuz:new({.1, .2, .3, .4})
        assert(A:intersection(Fuz.U) == A)
        assert(A:union(Fuz.U) == Fuz.U)

        assert(A:intersection(Fuz.E) == Fuz.E)
        assert(A:union(Fuz.E) == A)
    end
)


TS:test("symmetric difference",
    function ()
        local Fuz = FuzzyUniverse(4)

        local A = Fuz:new({.1, .2, .3, .4})
        local B = Fuz:new({.5, .6, .1, .2})

        -- Testing axioms after Vemur et al. (2014)
        --1. D(x, y) = D(y, x),
        --2. D(0, x) = x,
        --3. D(1, x) = N(x) where N is a fuzzy negation
        assert(A:symmetric_difference(B) == B:symmetric_difference(A))
        assert(A:symmetric_difference(Fuz.E) == A)
        assert(A:symmetric_difference(Fuz.U) == A:negation())
    end
)


os.exit(TS:run())
