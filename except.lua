Exception = {
    kind = {
        Base = "base exception",
        Access = "access exception",
        Logic = "logic exception",
    },
    __tostring = function (t)
        return string.format("%s: %s, %s",t.kind, t.message, debug.traceback("", 5))
    end
}

function Exception:new (kind, msg)
    return setmetatable({
        message = msg,
        kind = kind
    }, self)
end

function throw(msg)
    if type(msg) == "string"
    then
        msg = Exception:new(Exception.kind.Base, msg)
    end
    error(msg, 2)
end